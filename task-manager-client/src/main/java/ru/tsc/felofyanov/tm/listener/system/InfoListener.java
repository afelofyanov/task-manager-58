package ru.tsc.felofyanov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.event.ConsoleEvent;
import ru.tsc.felofyanov.tm.util.NumberUtil;

@Component
public final class InfoListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String getName() {
        return "info";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-i";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show system info.";
    }

    @Override
    @EventListener(condition = "@infoListener.getName() == #event.name || @infoListener.getArgument() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[INFO]");
        @NotNull final Runtime runtime = Runtime.getRuntime();
        System.out.println("Available processors (cores): " + runtime.availableProcessors());

        final long freeMemory = runtime.freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));

        final long maxMemory = runtime.maxMemory();
        final boolean isMemoryLimit = maxMemory == Long.MAX_VALUE;
        @NotNull final String memoryValue = isMemoryLimit ? "no limit" : NumberUtil.formatBytes(maxMemory);
        System.out.println("Maximum memory: " + memoryValue);

        final long totalMemory = runtime.totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));

        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory in JVM: " + NumberUtil.formatBytes(usedMemory));
    }
}
