package ru.tsc.felofyanov.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.tsc.felofyanov.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.felofyanov.tm.api.service.dto.IProjectDTOService;
import ru.tsc.felofyanov.tm.dto.model.ProjectDTO;

@Service
@AllArgsConstructor
public class ProjectDTOService extends AbstractUserOwnedDTOService<ProjectDTO, IProjectDTORepository>
        implements IProjectDTOService {

    @NotNull
    @Override
    protected IProjectDTORepository getRepository() {
        return context.getBean(IProjectDTORepository.class);
    }
}
