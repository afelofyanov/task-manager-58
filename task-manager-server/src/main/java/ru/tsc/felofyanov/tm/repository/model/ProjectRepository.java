package ru.tsc.felofyanov.tm.repository.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.felofyanov.tm.api.repository.model.IProjectRepository;
import ru.tsc.felofyanov.tm.model.Project;

import javax.persistence.EntityManager;

@Getter
@Repository
@Scope("prototype")
public class ProjectRepository extends AbstractUserOwnerRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull EntityManager entityManager) {
        super(entityManager, Project.class);
    }
}
