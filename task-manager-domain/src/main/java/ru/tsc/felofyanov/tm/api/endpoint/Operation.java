package ru.tsc.felofyanov.tm.api.endpoint;

import ru.tsc.felofyanov.tm.dto.request.AbstractRequest;
import ru.tsc.felofyanov.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);
}
